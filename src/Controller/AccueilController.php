<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Article;
use App\Form\ArticlesType;
use App\Repository\ArticlesRepository;



class AccueilController extends AbstractController
{
/**
   * @Route("/", name = "accueil")
   */
  public function index(ArticlesRepository $repo)
  {
    $result = $repo->getAll(); 

    return $this->render("accueil.html.twig", ['controller_name' => 'AffichArtController
  ', 'result'=> $result ]);  
  }

}