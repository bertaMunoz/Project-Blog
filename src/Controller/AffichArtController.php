<?php

namespace App\Controller;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticlesRepository;
use App\Entity\Article; 

class AffichArtController extends Controller   //affiche les articles
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(ArticlesRepository $repo){

        $result = $repo->getAll(); 

        return $this->render('admin.html.twig', [
            'controller_name' => 'AffichArtController
        ', 'result'=> $result  
        ]);
    }
    
    /**
     * @Route("/articleOne/{id}", name="articleOne")
     */
    
    public function showArticle(int $id, ArticlesRepository $repo){
        $result = $repo->get($id);
        return $this->render("article.html.twig", ['result'=>$result]);
    }
    
    /**
     * @Route("/liste", name="listArt")
     */
    public function afficheListCompl(ArticlesRepository $repo){

        $result = $repo->getAll(); 
        // $result = $repo->get($category);

        return $this->render('listeArt.html.twig', [
            'controller_name' => 'AffichArtController
        ', 'result'=> $result  
        ]);
    
    }

    /**
     * @Route("/cat/{category}", defaults={"category" = 0}, name="cat")
     */
    public function afficheParCat(int $category, ArticlesRepository $repo){

        $result = $repo->getOne($category); 
        // $result = $repo->get($category);

        return $this->render('articlesParCategories.html.twig', [
            'controller_name' => 'AffichArtController
        ', 'result'=> $result  
        ]);
}
    /**
     * @Route("/aPropos", name="apropos")
     */
    public function aPropos(){
        return $this->render('apropos.html.twig');
    }

     /**
     * @Route("/legal", name="legal")
     */
    public function legal(){
        return $this->render('legal.html.twig');
    }

     /**
     * @Route("/contact", name="contact")
     */
    public function contact(){
        return $this->render('contact.html.twig');
    }
}