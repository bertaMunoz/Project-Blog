<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\CreationCompte;
use App\Entity\Admin;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class CreationCompteController extends AbstractController
{
/**
   * @Route("/creercompte", name = "createAccount")
   */
  public function createAccount(Request $request)
  {
     $user = new Admin();
     $form = $this->createFormBuilder($user)
     ->add('name', TextType::class)
     ->add('email', TextType::class)
     ->add('password', TextType::class)
     ->add('save', SubmitType::class, array('label' => 'créer une compte'))
     ->getForm();

     $form->handleRequest($request);

     if ($form->isSubmitted() && $form->isValid()) {
     
      $car = $form->getData();

    return $this->render("creationCompte.html.twig", []);
  }
  return $this->render('creationCompte.html.twig', [
    'form' => $form->createView()]);

} 
}



 
