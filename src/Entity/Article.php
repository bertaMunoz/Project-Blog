<?php

namespace App\Entity; 

class Article{
 public $id;
 public $title;
 public $author;
 public $date;
 public $published;
 public $category; 
 public $content;
 public $url;

 public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->author = $sql["author"];
    $this->date = new \DateTime($sql["date"]);
    $this->published = $sql["published"];
    $this->category = $sql["category"];
    $this->content = $sql["content"];
    $this->url = $sql["url"];

    }

}
