<?php

namespace App\Repository;

use App\Entity\Article;


class ArticlesRepository   
{

  public function getAll() : array
  {
    $articles = [];
    try {
      dump($_ENV);
      // $cnx = new \PDO($_ENV["DATABASE_URL"]);
    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); 

      $query = $cnx->prepare("SELECT * FROM articles"); 
      $query->execute(); 
      foreach ($query->fetchAll() as $row) { 
      
        $article = new Article(); 
        $article->fromSQL($row);
        $articles[] = $article;
        
      }
    } catch (\PDOException $e) {

      dump($e); 
    }
    return $articles;
  }
  public function add(Article $articles)
  { 
    try {
      // $cnx = new \PDO($_ENV["DATABASE_URL"]);
    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("INSERT INTO articles(title, date, author, published, category, content, url ) VALUES(:title, :date, :author, :published, :category, :content, :url)");

      $query->bindValue(":title", $articles->title); 
      $query->bindValue(":date", $articles->date->format("Y-m-d"));
      $query->bindValue(":author", $articles->author);
      $query->bindValue(":published", $articles->published);
      $query->bindValue(":category", $articles->category);
      $query->bindValue(":content", $articles->content);
      $query->bindValue(":url", $articles->url);



      $query->execute();

      $articles->id = intval($cnx->lastInsertId()); //récupère le dernier id qui a été ajouté dan la base de donnée. Va ajouter le chien et récupérer l'id qui vient d'etre créé.  

    } catch (\PDOException $e) {

      dump($e);
    }
  }
  //tri par id //


  public function get($id): Article {
    $articles = [];
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      $query = $cnx->prepare("SELECT * FROM articles WHERE id = :id");
      $query-> bindValue(":id", $id); 
      $query->execute();

      foreach ($query->fetchAll()as $row){
        $article = new Article(); 
        $article->fromSQL($row);
        $articles[] = $article;
      }
    }catch (\PDOException $e){
      dump($e);
    }
    return $articles[0];
  }

  //tri par catégory// 

  public function getOne($category): array {
    $articles = [];
    try {
      $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
      $query = $cnx->prepare("SELECT * FROM articles WHERE category = :category");
      $query-> bindValue(":category", $category); 
      $query->execute();

      foreach ($query->fetchAll()as $row){
        $article = new Article(); 
        $article->fromSQL($row);
        $articles[] = $article;
      }
    }catch (\PDOException $e){
      dump($e);
    }
    return $articles;
  }
  
  public function update(Article $articles)
  { 
    try {
    $cnx = new \PDO("mysql:host=" . $_ENV["MYSQL_HOST"] . ":3306;dbname=" . $_ENV["MYSQL_DATABASE"], $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

      $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

      $query = $cnx->prepare("UPDATE articles SET title=:title, date= :date, author= :author, published= :published, category= :category, content= :content, url= :url  WHERE id= :id" );

      $query->bindValue(":title", $articles->title); 
      $query->bindValue(":date", $articles->date->format("Y-m-d"));
      $query->bindValue(":author", $articles->author);
      $query->bindValue(":published", $articles->published);
      $query->bindValue(":category", $articles->category);
      $query->bindValue(":content", $articles->content);
      $query->bindValue(":id", $articles->id);
      $query->bindValue(":url", $articles->url);


      return $query->execute();

    } catch (\PDOException $e) {

      throw $e;
    }
    return false;
  }

  public function delete(int $id) {
    try {
        $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

        $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        $query = $cnx->prepare("DELETE FROM articles WHERE id=:id");
        
        $query->bindValue(":id", $id);

        return $query->execute();

    } catch (\PDOException $e) {
        dump($e);
    }
    return false;
}


}
